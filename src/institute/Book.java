/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

/**
 * Class book
 * @author Adeel Khilji
 */
public class Book 
{
    private String title, author;//Declaring instance variables
    /**
     * Constructor with two parameters
     * @param title String
     * @param author String
     */
    Book(String title, String author)
    {
        this.title = title;//assigning title to this.title
        this.author = author;//assigning author to this.author
    }
    /**
     * getTitle() - getter method for this.title
     * @return String
     */
    public String getTitle()
    {
        return this.title;
    }
    /**
     * getAuthor() - getter method for this.author
     * @return String
     */
    public String getAuthor()
    {
        return this.author;
    }
}
