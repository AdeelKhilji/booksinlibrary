/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

import java.util.ArrayList;
import java.util.List;

/**
 * Testing Class
 * @author Adeel Khilji
 */
public class Composition 
{
    public static void main(String[] args)
    {
        List<Book> books = new ArrayList<Book>();
        Book book = new Book("Beyond the dark portal","Richard Knaak");
        books.add(book);
        book = new Book("Icewindale","R.A Salvator");
        books.add(book);
        
        Library library = new Library("Sheridan Library", books);
        
        System.out.println("LIBRARY:\n" + library.getName() + "\nBOOKS:");
        library.Display();
    }
}
