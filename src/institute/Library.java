/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

import java.util.List;

/**
 * Class Library
 * @author Adeel khilji
 */
public class Library 
{
    private String name;//Declaring instance variable name
    private List<Book> books;//Declaring object Book
    
    /**
     * Constructor with two parameters
     * @param name String
     * @param books Book
     */
    Library(String name, List<Book> books)
    {
        this.name = name;
        this.books = books;
    }
    /**
     * getName() - getter method for name
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
    /**
     * getBooks() - getter method for books
     * @return List<Book>
     */
    public List<Book> getBooks()
    {
        return this.books;
    }
    
    /**
     * Displaying list of books
     */
    public void Display()
    {
        for(Book b: books)
        {
            System.out.println("TITLE: " + b.getTitle() + "\tAUTHOR: " + b.getAuthor());
        }
    }
}
